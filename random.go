package main

import (
	"github.com/gonum/matrix/mat64"
	"github.com/gonum/stat/distuv"
	"log"
	"math"
	"math/rand"
	"time"
)

func main() {
	start := time.Now()
	s := rand.New(rand.NewSource(1000))
	b := distuv.ChiSquared{K: 0.7, Src: s}
	x := distuv.ChiSquared.LogProb(b, 0.000000005)
	println(math.Exp(x))
	elapsed := time.Since(start)
	log.Printf("Binomial took %s", elapsed)
}

func printm(m *mat64.Dense) {
	r, c := m.Dims()
	for i := 0; i < r; i++ {
		for j := 0; j < c; j++ {
			print(m.At(i, j), "\t")
		}
		println("")
	}
}
