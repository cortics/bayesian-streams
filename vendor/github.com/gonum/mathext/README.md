# mathext  [![Build Status](https://travis-ci.org/gonum/mathext.svg?branch=master)](https://travis-ci.org/gonum/mathext)  [![Coverage Status](https://coveralls.io/repos/github/gonum/mathext/badge.svg?branch=master)](https://coveralls.io/github/gonum/mathext?branch=master) [![GoDoc](https://godoc.org/github.com/gonum/mathext?status.svg)](https://godoc.org/github.com/gonum/mathext)

# This repository is no longer maintained. Development has moved to https://github.com/gonum/gonum.

mathext implements basic elementary functions not included in the Go standard library
